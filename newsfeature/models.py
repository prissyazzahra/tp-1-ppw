from django.db import models

class News(models.Model):
	title = models.CharField(max_length=75)
	image = models.CharField(max_length=200)
	date = models.DateField()
	isi = models.CharField(max_length=2000)
	read = models.CharField(max_length=5000)

# Create your models here.
