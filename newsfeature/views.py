from django.shortcuts import render, redirect
from .models import News

def home(request):
	response = {}
	allNews = News.objects.all()
	response['allNews'] = allNews
	return render(request, 'newspage.html', response)

def redirecting(request):
	return redirect('/home/')


# Create your views here.
