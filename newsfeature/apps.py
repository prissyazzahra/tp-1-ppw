from django.apps import AppConfig


class NewsfeatureConfig(AppConfig):
    name = 'newsfeature'
