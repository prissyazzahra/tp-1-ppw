from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import News

class StudentUnion_Test(TestCase):
	def test_student_union_news_exist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 200)

	def test_student_union_redirects_to_home(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 302)

	def test_landing_has_news_models(self):
		News.objects.create(title="Test", image="https://picsum.photos/400/300/?random", date="2001-01-01", isi="Test", read="https://google.com")
		count_news = News.objects.all().count()
		self.assertEqual(count_news, 1)

	


# Create your tests here.
