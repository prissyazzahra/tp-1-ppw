"""studentunion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from newsfeature.views import home, redirecting
from formevent.views import eventform
from memberform.views import signup, check_user_authenticated, logoutview
from displaynews.views import event
from aboutpage.views import about
from displaynews.views import event
from displaynews.views import history

urlpatterns = [
    path('admin/', admin.site.urls),
	path('eventform/<str:title>', eventform, name="eventform"),
    path('home/', home, name="home"),
    path('signup/',include('memberform.urls')),
    path('event/', event, name="event"),
    path('check_user_authenticated/', check_user_authenticated, name="check_user_authenticated"),
    path('logoutview/', logoutview, name = 'logoutview'),
    path('about/', include('aboutpage.urls')),
    path('history/', history, name="history"),
    path('', redirecting, name="redirecting")
]