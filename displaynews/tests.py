from django.test import TestCase
from django.urls import resolve
from django.test import Client
from .views import event
from .models import Events

# Create your tests here.
class DisplayNewsTest(TestCase):
	def test_event_is_exist(self):
		response = Client().get('/event/')
		self.assertEqual(response.status_code, 200)

	def test_news_is_not_exist(self):
		response = Client().get('/eventshow/')
		self.assertEqual(response.status_code, 404)

	def test_news_can_create_event_object(self):
		my_obj = Events.objects.create(title="apa", image="hai", date="2010-02-02", description="hi")
		self.assertEqual(Events.objects.all().count(), 1)

	def test_history_is_exist(self):
		response = Client().get('/history/')
		self.assertEqual(response.status_code, 200)