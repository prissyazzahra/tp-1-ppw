from django.db import models

# Create your models here.
class Events(models.Model):
	title = models.CharField(max_length=50)
	image = models.CharField(max_length=100)
	date = models.DateField()
	description = models.CharField(max_length=2000)