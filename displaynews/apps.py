from django.apps import AppConfig


class DisplaynewsConfig(AppConfig):
    name = 'displaynews'