from django.shortcuts import render
from .models import Events
from formevent.models import EventList

# Create your views here.
response = {}
def event(request):
	allEvents = Events.objects.all()
	response['allEvents'] = allEvents
	return render(request, 'news.html', response)

def history(request):
	if "user_id" in request.session.keys():
		email = request.session['email']
		print(email)
		datas = []
		events = EventList.objects.filter(email=email)
		print(events)
		# for i in events:
		# 	if(i.email == email):
		# 		datas.append(i)
		# content = {
		# 	'datas' : datas,
		# 	'events' : events,
		# 	'banyak_event' : len(datas)
		# }
		response['events'] = events
		response['event_number'] = events.count()

		# print(datas)
		# response['is_authenticated'] = True
		# response['data'] = datas
		return render(request, 'history.html', response)

	return render(request, 'member.html')