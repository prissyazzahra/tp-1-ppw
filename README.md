# Tugas 1 PPW 2018: Student Union
## Nama Anggota Kelompok 8 PPW Kelas F
Prissy Azzahra Ratnadwita - 1706984700 (news page)<br /> 
Angelina Condro - 1706043342 (event form)<br />
Yudha Ananda Rahayu Putra - 1706984796 (member form)<br />
Bintang Agung Nusantara - 1706043550 (events page)<br />

# Status pipelines dan code coverage
[![pipeline status](https://gitlab.com/prissyazzahra/tp-1-ppw/badges/master/pipeline.svg)](https://gitlab.com/prissyazzahra/tp-1-ppw/commits/master)
[![coverage report](https://gitlab.com/prissyazzahra/tp-1-ppw/badges/master/coverage.svg)](https://gitlab.com/prissyazzahra/tp-1-ppw/commits/master)

# Link herokuapp
http://studentunion-kel8.herokuapp.com
