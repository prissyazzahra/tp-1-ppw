from django import forms
from .models import membermodel


class memberform(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    namalengkap = forms.CharField(label = "Nama lengkap", max_length= 100, required=True, widget = forms.TextInput(attrs = attrs))
    username = forms.CharField(label = "Username", max_length=50, required=True, widget = forms.TextInput(attrs = attrs))
    email = forms.EmailField(label = "Email", max_length=70, required=True, widget = forms.TextInput(attrs = attrs))
    password = forms.CharField(label = "Password", max_length = 50, required=True, widget = forms.PasswordInput(attrs = attrs))
    tglLahir = forms.DateField(label = "Tanggal lahir", required = True, widget = forms.DateInput(attrs = {"type" : "date"}))
    alamat = forms.CharField(label = "Alamat lengkap", max_length = 100, required=False, widget = forms.TextInput(attrs = attrs))