from django.urls import path
from .views import signup, logoutview

urlpatterns = [
	path('',signup, name = 'signup'),
	path('logoutview', logoutview, name = 'signout')
]