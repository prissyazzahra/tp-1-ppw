from django.db import models
from django.utils import timezone
from datetime import date, time
from .validators import isalphavalidator

class membermodel(models.Model):
	namalengkap = models.CharField(max_length=100)
	username = models.CharField(max_length=50)
	email = models.EmailField(max_length=70)
	password = models.CharField(max_length = 50, validators=[isalphavalidator])
	tglLahir = models.DateField(default= timezone.now)
	alamat = models.CharField(max_length = 100)