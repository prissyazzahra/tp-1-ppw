from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import signup, logoutview
from .models import membermodel
from .forms import memberform

# Create your tests here.
class MemberFormUnitTest(TestCase):

    def test_signup_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)

    def test_logout(self):
        response = Client().get('/signup/logoutview')
        self.assertEqual(response.status_code, 302)

    def test_signup_url_is_redirect(self):
        response = self.client.get('/signup')
        self.assertEqual(response.status_code, 301)

    def test_signup_url_is_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)

    # def test_model_can_create_data(self):
    #     data = membermodel.objects.create(namalengkap = "siapa hayo", username = "user", email = "email@email.com",
    #         password = "pw", tglLahir = "2222-02-22", alamat = "dunia")
    #     counting_all_input = membermodel.objects.all().count()
    #     self.assertEqual(counting_all_input, 1)

    # def test_get_char_in_page(self):
    # 	response = Client().get('/signup/')
    # 	html_response = response.content.decode('utf8')
    # 	self.assertIn('Register Here', html_response)