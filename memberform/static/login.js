$(document).ready(function() {
	$.ajax({
		url: "/check_user_authenticated/",
        success: function(result) {
            if (result.is_authenticated) {
				console.log('User is logged in');
				console.log('masuk');
				var userEntity = JSON.parse(sessionStorage.getItem('myUserEntity'));
				var name = userEntity.Name;
                $('#signup').replaceWith('<a class="nav-link" id="signout" href="#" onclick="signOut()">SIGN OUT</a>');
				$('#profile').replaceWith('<a class="nav-link" id="profile" href="/history">PROFILE</a>');
				$('.testiform').css('display', "block");
				$('#sapaan').replaceWith('<h4 id = "sapaan">Hello, ' + name + '!</h4>');
            } else {
                console.log('User is logged out');
                $('#signout').replaceWith('<a id="signup" class="nav-link"  href="{% url "signup" %}">SIGN IN</a>');
				$('#profile').replaceWith('<a class="nav-link" id="profile" href="#"></a>');
				$('.testiform').css('display', "none");
				$('#sapaan').replaceWith("<h4 id = 'sapaan'></h4>");
            }
		},
		error: function(result){
			console.log("Error")
		}
    });
});

// $(document).ready(function() {
// 	alert("bla");
// });

function onSignIn(googleUser) {
	var id_token = googleUser.getAuthResponse().id_token;
	sendToken(id_token);
	var profile = googleUser.getBasicProfile();
	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	console.log('Name: ' + profile.getName());
	console.log('Image URL: ' + profile.getImageUrl());
	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

	var myUserEntity = {};
	myUserEntity.Id = profile.getId();
	myUserEntity.Name = profile.getName();
	myUserEntity.Email = profile.getEmail();

	//Store the entity object in sessionStorage where it will be accessible from all pages of your site.
	sessionStorage.setItem('myUserEntity',JSON.stringify(myUserEntity));
	alert('Halo ' + profile.getName());
}

function checkIfLoggedIn() {
	if(sessionStorage.getItem('myUserEntity') == null){
    //Redirect to login page, no user entity available in sessionStorage
    //window.location.href='member.html';
} else {
    //User already logged in
    var userEntity = {};
    userEntity = JSON.parse(sessionStorage.getItem('myUserEntity'));
  }
}

function init(){
	gapi.load('auth2', function(){
		gapi.auth2.init();
		console.log('loaded');
	});
}


function sendToken(token) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var html;

    $.ajax({
        method: "POST",
        url: "/signup/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {id_token: token},
        success: function(result) {
            console.log("Successfully send token");
            if (result.status === "0") {
                console.log("Successfully logged in");
                window.location.replace(result.url);
            } else {
                console.log("Something went wrong.");
            }
        },
        error: function () {
            alert("Something error, please report")
        }
    })
}

function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function () {
		sessionStorage.clear();
		console.log('User signed out.');
	});
	$.ajax({
		url : "/logoutview/",
		success : function(result) {
			location.reload();
		}
	});
}
