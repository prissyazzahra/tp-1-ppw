from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from .forms import UserComments
from .models import Testimony

# Create your views here.
response = {}

def about(request):
	form = UserComments(request.POST or None)
	if(request.method == "POST"):
		if form.is_valid():
			email = request.session["email"]
			testi = request.POST["testi"]
			obj = Testimony.objects.create(email=email, testi=testi)
			time = obj.created_at.strftime("%b. %d, %Y, %I:%M %p")
			return JsonResponse({'testi':testi, 'email':email, 'time': time})
	else:
		allComments = Testimony.objects.all()
		response['form'] = form
		response['allComments'] = allComments
		return render(request, 'about.html', response)
