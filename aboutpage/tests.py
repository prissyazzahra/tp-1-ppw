from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, get_user_model
from .views import *
from .models import Testimony
from .forms import UserComments

# Create your tests here.

class About_Test(TestCase):
	def test_about_page_exists(self):
		response = Client().get('/about/')
		self.assertEqual(response.status_code, 200)

	def test_about_page_can_create_testimony(self):
		Testimony.objects.create(email="johndoe@example.com", testi="Hello hello thank you for coming", created_at="2018-02-02")
		count = Testimony.objects.all().count()
		self.assertEqual(count, 1)

	def setUp(self):
		self.session = self.client.session
		self.session['email'] = 'test@test.com'
		self.session.save()
		# User = get_user_model()
		# userobj = User.objects.create_user("Test", "test@test.com", "123456")

	def test_about_page_post_success_and_render_result(self):
		email = self.session['email']
		test = "This is a testimony."
		response_post = self.client.post('/about/', {'testi': test, 'email': email})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('/about/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)
