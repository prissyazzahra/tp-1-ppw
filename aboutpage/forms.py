from django import forms
from .models import Testimony

class UserComments(forms.Form):
	testi = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control p-5', 'placeholder': 'Enter a testimony...'}), max_length=300)