from django.db import models
from django.utils import timezone

class Testimony(models.Model):
	email = models.EmailField(max_length=100, editable=False, null=False, blank=False)
	testi = models.CharField(max_length=300)
	created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)