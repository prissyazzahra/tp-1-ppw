from django.conf.urls import url
from django.urls import path
from aboutpage.views import about

urlpatterns = [
	path('', about, name="about"),
]