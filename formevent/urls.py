from django.conf.urls import url, path
from .views import eventform


urlpatterns = [
	path('/eventform/', eventform, name="eventform"),
	path('', eventform, name="eventform")
]