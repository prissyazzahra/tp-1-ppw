from django.db import models
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.utils import timezone

class EventList(models.Model):
	email = models.EmailField(max_length = 30)
	event = models.CharField(max_length = 30)
	date = models.DateField(default= timezone.now)