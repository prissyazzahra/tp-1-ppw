# Generated by Django 2.1.1 on 2018-12-13 17:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formevent', '0005_auto_20181213_2147'),
    ]

    operations = [
        migrations.CreateModel(
            name='joinEventMember',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='joinEventNonMember',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('emaill', models.EmailField(max_length=30)),
                ('username', models.CharField(max_length=30)),
                ('password', models.CharField(max_length=30)),
            ],
        ),
    ]
