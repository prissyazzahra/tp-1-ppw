$(function() {
	$("#submit").click(function() {
		event.preventDefault();
		var csrftoken = $("[name=csrfmiddlewaretoken]").val();
		$.ajax({
			headers: {"X-CSRFToken": csrftoken},
			url: "/about/",
			data: $("form").serialize(),
			method:  'POST',
			dataType: 'json',
			success: function(data){
                alert("Your response has been recorded!")
                var msg = '<h4>' + data.email + '</h4><h5>Posted on ' + data.time + '</h5>' + '<p>' + data.testi + '</p>';
                $(".messages").append(msg);
                window.location.replace("/about/");
            }, error: function() {
                alert("Error, cannot get data from server")
            }
		})
	});
});

function getUserEmail() {
	
}